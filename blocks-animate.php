<?php
/**
 * Plugin Name: Blocks Animate
 * Description: Adds animation settings based on animate.css.
 * Author: Chet Hardin
 * Author URI: https://chethardin.com
 * Version: 1.0.0
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: blocks-animate
 * Domain Path: /languages/
 *
 * @package blocks-animate
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

add_action( 'enqueue_block_editor_assets', 'blocks_animate_enqueue_block_editor_assets' );

function blocks_animate_enqueue_block_editor_assets() {
    wp_enqueue_script('blocks-animate-js',esc_url( plugins_url( '/dist/blocks-animate.js', __FILE__ ) ),array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ), '1.0.0', true);
}

add_action( 'init', 'blocks_animate_enqueue_assets' );

function blocks_animate_enqueue_assets() {
    wp_enqueue_script('blocks-animate-script',	plugins_url( 'dist/script.js', __FILE__ ), array(), '1.0.0');
    wp_enqueue_style('blocks-animate-css', plugins_url( 'dist/styles.min.css', __FILE__ ), '1.0.0');
	wp_enqueue_style('animate-css', '//cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css', array(),	'4.0.0');
}
