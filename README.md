# Add easy to use animations to blocks

### Features
- Postpone animations until dom is loaded. 
- Utilizes intersection observer to load styles when target element in viewport. 

Animation styles courtesy of [animate.css by Daniel Eden](https://animate.style/).
Headstart provided by [Jürg Hunziker](https://github.com/liip/extend-block-example-wp-plugin). 