import { styleControlOptions, speedControlOptions, delayControlOptions } from './blocks-animate';
/**
 * Add custom attribute for mobile visibility.
 * @param {Object} settings Settings for the block.
 * @return {Object} settings Modified settings.
 */
export function addAttributes( settings ) {
	if ( typeof settings.attributes !== 'undefined' ) {
		settings.attributes = Object.assign( settings.attributes, {
			visibleOnMobile: {
				type: 'boolean',
				default: true,
			},
			style: {
				style: 'string',
				default: styleControlOptions[ 0 ].value,
			},
			speed: {
				style: 'string',
				default: speedControlOptions[ 0 ].value,
			},
			delay: {
				style: 'string',
				default: delayControlOptions[ 0 ].value,
			},
		} );
	}
	return settings;
}
