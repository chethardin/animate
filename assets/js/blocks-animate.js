import classnames from 'classnames';
import { addAttributes } from './addAttributes';
const { __ } = wp.i18n;
const { addFilter } = wp.hooks;
const { Fragment }	= wp.element;
const { InspectorControls } = wp.editor;
const { createHigherOrderComponent } = wp.compose;
const { PanelBody, SelectControl } = wp.components;

export const speedControlOptions = [
		{ label: __( 'Default' ), value: '' },
		{ label: __( 'Slower' ), value: 'slower' },
		{ label: __( 'Slow' ), value: 'slow' },
		{ label: __( 'Fast' ), value: 'fast' },
		{ label: __( 'Faster' ), value: 'faster' },
	],
	delayControlOptions = [
		{ label: __( 'Default' ), value: '' },
		{ label: __( '100 milliseconds' ), value: 'delay-100ms' },
		{ label: __( '200 milliseconds' ), value: 'delay-200ms' },
		{ label: __( '500 milliseconds' ), value: 'delay-500ms' },
		{ label: __( 'One second' ), value: 'delay-1s' },
		{ label: __( 'Two seconds' ), value: 'delay-2s' },
		{ label: __( 'Three seconds' ), value: 'delay-3s' },
		{ label: __( 'Four seconds' ), value: 'delay-4s' },
		{ label: __( 'Five seconds' ), value: 'delay-5s' },
	],
	styleControlOptions = [
		{
			label: __( 'Attention Seekers' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Bounce' ), value: 'bounce' },
		{ label: __( 'Flash' ), value: 'flash' },
		{ label: __( 'Pulse' ), value: 'pulse' },
		{ label: __( 'Rubber Band' ), value: 'rubberBand' },
		{ label: __( 'ShakeX' ), value: 'shakeX' },
		{ label: __( 'ShakeY' ), value: 'shakeY' },
		{ label: __( 'Head Shake' ), value: 'headShake' },
		{ label: __( 'Swing' ), value: 'swing' },
		{ label: __( 'TaDa' ), value: 'tada' },
		{ label: __( 'Wobble' ), value: 'wobble' },
		{ label: __( 'Jello' ), value: 'jello' },		
		{ label: __( 'Heart Beat' ), value: 'heartBeat' },
		{
			label: __( 'Back Entrances' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Bounce In Down' ), value: 'bounceInDown' },
		{ label: __( 'Bounce In Left' ), value: 'bounceInLeft' },
		{ label: __( 'Bounce In Right' ), value: 'bounceInRight' },
		{ label: __( 'Bounce In Up' ), value: 'bounceInUp' },
		{
			label: __( 'Back Exits' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Bounce Out Down' ), value: 'bounceOutDown' },
		{ label: __( 'Bounce Out Left' ), value: 'bounceOutLeft' },
		{ label: __( 'Bounce Out Right' ), value: 'bounceOutRight' },
		{ label: __( 'Bounce Out Up' ), value: 'bounceOutUp' },
		{
			label: __( 'Bouncing Entrances' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Bounce In' ), value: 'bounceIn' },
		{ label: __( 'Bounce In Down' ), value: 'bounceInDown' },
		{ label: __( 'Bounce In Left' ), value: 'bounceInLeft' },
		{ label: __( 'Bounce In Right' ), value: 'bounceInRight' },
		{ label: __( 'Bounce In Up' ), value: 'bounceInUp' },
		{
			label: __( 'Bouncing Exits' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Bounce Out' ), value: 'bounceOut' },
		{ label: __( 'Bounce Out Down' ), value: 'bounceOutDown' },
		{ label: __( 'Bounce Out Left' ), value: 'bounceOutLeft' },
		{ label: __( 'Bounce Out Right' ), value: 'bounceOutRight' },
		{ label: __( 'Bounce Out Up' ), value: 'bounceOutUp' },
		{
			label: __( 'Fading Entrances' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Fade In' ), value: 'fadeIn' },
		{ label: __( 'Fade In Down' ), value: 'fadeInDown' },
		{ label: __( 'Fade In Left' ), value: 'fadeInLeft' },
		{ label: __( 'Fade In Right' ), value: 'fadeInRight' },
		{ label: __( 'Fade In Up' ), value: 'fadeInUp' },
		{ label: __( 'Fade In Top Left' ), value: 'fadeInTopLeft' },
		{ label: __( 'Fade In Top Right' ), value: 'fadeInTopRight' },
		{ label: __( 'Fade In Bottom Left' ), value: 'fadeInBottomLeft' },
		{ label: __( 'Fade In Bottom Right' ), value: 'fadeInBottomRight' },
		{
			label: __( 'Fading Exits' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Fade Out' ), value: 'fadeOut' },
		{ label: __( 'Fade Out Down' ), value: 'fadeOutDown' },
		{ label: __( 'Fade Out Left' ), value: 'fadeOutLeft' },
		{ label: __( 'Fade Out Right' ), value: 'fadeOutRight' },
		{ label: __( 'Fade Out Up' ), value: 'fadeOutUp' },
		{ label: __( 'Fade Out Top Left' ), value: 'fadeOutTopLeft' },
		{ label: __( 'Fade Out Top Right' ), value: 'fadeOutTopRight' },
		{ label: __( 'Fade Out Bottom Left' ), value: 'fadeOutBottomLeft' },
		{ label: __( 'Fade Out Bottom Right' ), value: 'fadeOutBottomRight' },
		{
			label: __( 'Flippers' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Flip' ), value: 'flip' },
		{ label: __( 'Flip In X' ), value: 'flipInX' },
		{ label: __( 'Flip In Y' ), value: 'flipInY' },
		{ label: __( 'Flip Out X' ), value: 'flipOutX' },
		{ label: __( 'Flip Out Y' ), value: 'flipOutY' },
		{
			label: __( 'Lightspeed' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Light Speed In Left' ), value: 'lightSpeedInLeft' },
		{ label: __( 'Light Speed In Right' ), value: 'lightSpeedInRight' },
		{ label: __( 'Light Speed Out Left' ), value: 'lightSpeedOutLeft' },
		{ label: __( 'Light Speed Out Right' ), value: 'lightSpeedOutRight' },
		{
			label: __( 'Rotating Entrances' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Rotate In' ), value: 'rotateIn' },
		{ label: __( 'Rotate In Down Left' ), value: 'rotateInDownLeft' },
		{ label: __( 'Rotate In Down Right' ), value: 'rotateInDownRight' },
		{ label: __( 'Rotate In Up Left' ), value: 'rotateInUpLeft' },
		{ label: __( 'Rotate In Up Right' ), value: 'rotateInUpRight' },
		{
			label: __( 'Rotating Exits' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Rotate Out' ), value: 'rotateOut' },
		{ label: __( 'Rotate Out Down Left' ), value: 'rotateOutDownLeft' },
		{ label: __( 'Rotate Out Down Right' ), value: 'rotateOutDownRight' },
		{ label: __( 'Rotate Out Up Left' ), value: 'rotateOutUpLeft' },
		{ label: __( 'Rotate Out Up Right' ), value: 'rotateOutUpRight' },
		{
			label: __( 'Specials' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Hinge' ), value: 'hinge' },
		{ label: __( 'Jack In The Box' ), value: 'jackInTheBox' },
		{ label: __( 'Roll In' ), value: 'rollIn' },
		{ label: __( 'Roll Out' ), value: 'rollOut' },
		{
			label: __( 'Zooming Entrances' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Zoom In' ), value: 'zoomIn' },
		{ label: __( 'Zoom In Down' ), value: 'zoomInDown' },
		{ label: __( 'Zoom In Left' ), value: 'zoomInLeft' },
		{ label: __( 'Zoom In Right' ), value: 'zoomInRight' },
		{ label: __( 'Zoom In Up' ), value: 'zoomInUp' },
		{
			label: __( 'Zooming Exits' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Zoom Out' ), value: 'zoomOut' },
		{ label: __( 'Zoom Out Down' ), value: 'zoomOutDown' },
		{ label: __( 'Zoom Out Left' ), value: 'zoomOutLeft' },
		{ label: __( 'Zoom Out Right' ), value: 'zoomOutRight' },
		{ label: __( 'Zoom Out Up' ), value: 'zoomOutUp' },
		{
			label: __( 'Sliding Entrances' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Slide In Down' ), value: 'slideInDown' },
		{ label: __( 'Slide In Left' ), value: 'slideInLeft' },
		{ label: __( 'Slide In Right' ), value: 'slideInRight' },
		{ label: __( 'Slide In Up' ), value: 'slideInUp' },
		{
			label: __( 'Sliding Exits' ),
			value: null,
			disabled: true,
		},
		{ label: __( 'Slide Out Down' ), value: 'slideOutDown' },
		{ label: __( 'Slide Out Left' ), value: 'slideOutLeft' },
		{ label: __( 'Slide Out Right' ), value: 'slideOutRight' },
		{ label: __( 'Slide Out Up' ), value: 'slideOutUp' },

	];

/**
 * Add mobile visibility controls on Advanced Block Panel.
 * @param {function} BlockEdit Block edit component.
 * @return {function} BlockEdit Modified block edit component.
 */
const withAdvancedControls = createHigherOrderComponent( ( BlockEdit ) => {
	return ( props ) => {
		const {
			attributes,
			setAttributes,
			isSelected,
		} = props;

		const {
			speed,
			delay,
			style,
		} = attributes;

		return (
			<Fragment>
				<BlockEdit { ...props } />
				{ isSelected &&
					<InspectorControls>
						<PanelBody
							title={ __( 'Animations' ) }
							initialOpen={ false }
						>
							<SelectControl
								label={ __( 'Style' ) }
								value={ style }
								options={ styleControlOptions }
								onChange={ ( selectedStyleOption ) => {
									setAttributes( {
										style: selectedStyleOption,
									} );
								} }
							/>
							<SelectControl
								label={ __( 'Speed' ) }
								value={ speed }
								options={ speedControlOptions }
								initialOpen={ false }
								onChange={ ( selectedSpeedOption ) => {
									setAttributes( {
										speed: selectedSpeedOption,
									} );
								} }
							/>
							<SelectControl
								label={ __( 'Delay' ) }
								value={ delay }
								options={ delayControlOptions }
								initialOpen={ false }
								onChange={ ( selectedDelayOption ) => {
									setAttributes( {
										delay: selectedDelayOption,
									} );
								} }
							/>
						</PanelBody>
					</InspectorControls>
				}
			</Fragment>
		);
	};
}, 'withAdvancedControls' );

/**
 * Add custom element class in save element.
 * @param {Object} extraProps     Block element.
 * @param {Object} blockType      Blocks object.
 * @param {Object} attributes     Blocks attributes.
 * @return {Object} extraProps Modified block element.
 */
function applyExtraClass( extraProps, blockType, attributes ) {

	const { speed, delay, style } = attributes;

	if ( typeof style !== 'undefined' && '' !== style ) {
		extraProps.className = classnames( extraProps.className, 'animate__animated animate__' + style );
	}
	if ( typeof speed !== 'undefined' && '' !== speed ) {
		extraProps.className = classnames( extraProps.className, 'animate__' + speed );
	}
	if ( typeof delay !== 'undefined' && '' !== delay ) {
		extraProps.className = classnames( extraProps.className, 'animate__' + delay );
	}

	return extraProps;
}

addFilter(
	'blocks.registerBlockType',
	'editorskit/custom-attributes',
	addAttributes
);

addFilter(
	'editor.BlockEdit',
	'editorskit/custom-advanced-control',
	withAdvancedControls
);

addFilter(
	'blocks.getSaveContent.extraProps',
	'editorskit/applyExtraClass',
	applyExtraClass
);
