// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"../../node_modules/classnames/index.js":[function(require,module,exports) {
var define;
/*!
  Copyright (c) 2017 Jed Watson.
  Licensed under the MIT License (MIT), see
  http://jedwatson.github.io/classnames
*/
/* global define */

(function () {
	'use strict';

	var hasOwn = {}.hasOwnProperty;

	function classNames () {
		var classes = [];

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (!arg) continue;

			var argType = typeof arg;

			if (argType === 'string' || argType === 'number') {
				classes.push(arg);
			} else if (Array.isArray(arg) && arg.length) {
				var inner = classNames.apply(null, arg);
				if (inner) {
					classes.push(inner);
				}
			} else if (argType === 'object') {
				for (var key in arg) {
					if (hasOwn.call(arg, key) && arg[key]) {
						classes.push(key);
					}
				}
			}
		}

		return classes.join(' ');
	}

	if (typeof module !== 'undefined' && module.exports) {
		classNames.default = classNames;
		module.exports = classNames;
	} else if (typeof define === 'function' && typeof define.amd === 'object' && define.amd) {
		// register as 'classnames', consistent with npm package name
		define('classnames', [], function () {
			return classNames;
		});
	} else {
		window.classNames = classNames;
	}
}());

},{}],"addAttributes.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addAttributes = addAttributes;

var _blocksAnimate = require("./blocks-animate");

/**
 * Add custom attribute for mobile visibility.
 * @param {Object} settings Settings for the block.
 * @return {Object} settings Modified settings.
 */
function addAttributes(settings) {
  if (typeof settings.attributes !== 'undefined') {
    settings.attributes = Object.assign(settings.attributes, {
      visibleOnMobile: {
        type: 'boolean',
        default: true
      },
      style: {
        style: 'string',
        default: _blocksAnimate.styleControlOptions[0].value
      },
      speed: {
        style: 'string',
        default: _blocksAnimate.speedControlOptions[0].value
      },
      delay: {
        style: 'string',
        default: _blocksAnimate.delayControlOptions[0].value
      }
    });
  }

  return settings;
}
},{"./blocks-animate":"blocks-animate.js"}],"blocks-animate.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styleControlOptions = exports.delayControlOptions = exports.speedControlOptions = void 0;

var _classnames = _interopRequireDefault(require("classnames"));

var _addAttributes = require("./addAttributes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var __ = wp.i18n.__;
var addFilter = wp.hooks.addFilter;
var Fragment = wp.element.Fragment;
var InspectorControls = wp.editor.InspectorControls;
var createHigherOrderComponent = wp.compose.createHigherOrderComponent;
var _wp$components = wp.components,
    PanelBody = _wp$components.PanelBody,
    SelectControl = _wp$components.SelectControl;
var speedControlOptions = [{
  label: __('Default'),
  value: ''
}, {
  label: __('Slower'),
  value: 'slower'
}, {
  label: __('Slow'),
  value: 'slow'
}, {
  label: __('Fast'),
  value: 'fast'
}, {
  label: __('Faster'),
  value: 'faster'
}],
    delayControlOptions = [{
  label: __('Default'),
  value: ''
}, {
  label: __('100 milliseconds'),
  value: 'delay-100ms'
}, {
  label: __('200 milliseconds'),
  value: 'delay-200ms'
}, {
  label: __('500 milliseconds'),
  value: 'delay-500ms'
}, {
  label: __('One second'),
  value: 'delay-1s'
}, {
  label: __('Two seconds'),
  value: 'delay-2s'
}, {
  label: __('Three seconds'),
  value: 'delay-3s'
}, {
  label: __('Four seconds'),
  value: 'delay-4s'
}, {
  label: __('Five seconds'),
  value: 'delay-5s'
}],
    styleControlOptions = [{
  label: __('Attention Seekers'),
  value: null,
  disabled: true
}, {
  label: __('Bounce'),
  value: 'bounce'
}, {
  label: __('Flash'),
  value: 'flash'
}, {
  label: __('Pulse'),
  value: 'pulse'
}, {
  label: __('Rubber Band'),
  value: 'rubberBand'
}, {
  label: __('ShakeX'),
  value: 'shakeX'
}, {
  label: __('ShakeY'),
  value: 'shakeY'
}, {
  label: __('Head Shake'),
  value: 'headShake'
}, {
  label: __('Swing'),
  value: 'swing'
}, {
  label: __('TaDa'),
  value: 'tada'
}, {
  label: __('Wobble'),
  value: 'wobble'
}, {
  label: __('Jello'),
  value: 'jello'
}, {
  label: __('Heart Beat'),
  value: 'heartBeat'
}, {
  label: __('Back Entrances'),
  value: null,
  disabled: true
}, {
  label: __('Bounce In Down'),
  value: 'bounceInDown'
}, {
  label: __('Bounce In Left'),
  value: 'bounceInLeft'
}, {
  label: __('Bounce In Right'),
  value: 'bounceInRight'
}, {
  label: __('Bounce In Up'),
  value: 'bounceInUp'
}, {
  label: __('Back Exits'),
  value: null,
  disabled: true
}, {
  label: __('Bounce Out Down'),
  value: 'bounceOutDown'
}, {
  label: __('Bounce Out Left'),
  value: 'bounceOutLeft'
}, {
  label: __('Bounce Out Right'),
  value: 'bounceOutRight'
}, {
  label: __('Bounce Out Up'),
  value: 'bounceOutUp'
}, {
  label: __('Bouncing Entrances'),
  value: null,
  disabled: true
}, {
  label: __('Bounce In'),
  value: 'bounceIn'
}, {
  label: __('Bounce In Down'),
  value: 'bounceInDown'
}, {
  label: __('Bounce In Left'),
  value: 'bounceInLeft'
}, {
  label: __('Bounce In Right'),
  value: 'bounceInRight'
}, {
  label: __('Bounce In Up'),
  value: 'bounceInUp'
}, {
  label: __('Bouncing Exits'),
  value: null,
  disabled: true
}, {
  label: __('Bounce Out'),
  value: 'bounceOut'
}, {
  label: __('Bounce Out Down'),
  value: 'bounceOutDown'
}, {
  label: __('Bounce Out Left'),
  value: 'bounceOutLeft'
}, {
  label: __('Bounce Out Right'),
  value: 'bounceOutRight'
}, {
  label: __('Bounce Out Up'),
  value: 'bounceOutUp'
}, {
  label: __('Fading Entrances'),
  value: null,
  disabled: true
}, {
  label: __('Fade In'),
  value: 'fadeIn'
}, {
  label: __('Fade In Down'),
  value: 'fadeInDown'
}, {
  label: __('Fade In Left'),
  value: 'fadeInLeft'
}, {
  label: __('Fade In Right'),
  value: 'fadeInRight'
}, {
  label: __('Fade In Up'),
  value: 'fadeInUp'
}, {
  label: __('Fade In Top Left'),
  value: 'fadeInTopLeft'
}, {
  label: __('Fade In Top Right'),
  value: 'fadeInTopRight'
}, {
  label: __('Fade In Bottom Left'),
  value: 'fadeInBottomLeft'
}, {
  label: __('Fade In Bottom Right'),
  value: 'fadeInBottomRight'
}, {
  label: __('Fading Exits'),
  value: null,
  disabled: true
}, {
  label: __('Fade Out'),
  value: 'fadeOut'
}, {
  label: __('Fade Out Down'),
  value: 'fadeOutDown'
}, {
  label: __('Fade Out Left'),
  value: 'fadeOutLeft'
}, {
  label: __('Fade Out Right'),
  value: 'fadeOutRight'
}, {
  label: __('Fade Out Up'),
  value: 'fadeOutUp'
}, {
  label: __('Fade Out Top Left'),
  value: 'fadeOutTopLeft'
}, {
  label: __('Fade Out Top Right'),
  value: 'fadeOutTopRight'
}, {
  label: __('Fade Out Bottom Left'),
  value: 'fadeOutBottomLeft'
}, {
  label: __('Fade Out Bottom Right'),
  value: 'fadeOutBottomRight'
}, {
  label: __('Flippers'),
  value: null,
  disabled: true
}, {
  label: __('Flip'),
  value: 'flip'
}, {
  label: __('Flip In X'),
  value: 'flipInX'
}, {
  label: __('Flip In Y'),
  value: 'flipInY'
}, {
  label: __('Flip Out X'),
  value: 'flipOutX'
}, {
  label: __('Flip Out Y'),
  value: 'flipOutY'
}, {
  label: __('Lightspeed'),
  value: null,
  disabled: true
}, {
  label: __('Light Speed In Left'),
  value: 'lightSpeedInLeft'
}, {
  label: __('Light Speed In Right'),
  value: 'lightSpeedInRight'
}, {
  label: __('Light Speed Out Left'),
  value: 'lightSpeedOutLeft'
}, {
  label: __('Light Speed Out Right'),
  value: 'lightSpeedOutRight'
}, {
  label: __('Rotating Entrances'),
  value: null,
  disabled: true
}, {
  label: __('Rotate In'),
  value: 'rotateIn'
}, {
  label: __('Rotate In Down Left'),
  value: 'rotateInDownLeft'
}, {
  label: __('Rotate In Down Right'),
  value: 'rotateInDownRight'
}, {
  label: __('Rotate In Up Left'),
  value: 'rotateInUpLeft'
}, {
  label: __('Rotate In Up Right'),
  value: 'rotateInUpRight'
}, {
  label: __('Rotating Exits'),
  value: null,
  disabled: true
}, {
  label: __('Rotate Out'),
  value: 'rotateOut'
}, {
  label: __('Rotate Out Down Left'),
  value: 'rotateOutDownLeft'
}, {
  label: __('Rotate Out Down Right'),
  value: 'rotateOutDownRight'
}, {
  label: __('Rotate Out Up Left'),
  value: 'rotateOutUpLeft'
}, {
  label: __('Rotate Out Up Right'),
  value: 'rotateOutUpRight'
}, {
  label: __('Specials'),
  value: null,
  disabled: true
}, {
  label: __('Hinge'),
  value: 'hinge'
}, {
  label: __('Jack In The Box'),
  value: 'jackInTheBox'
}, {
  label: __('Roll In'),
  value: 'rollIn'
}, {
  label: __('Roll Out'),
  value: 'rollOut'
}, {
  label: __('Zooming Entrances'),
  value: null,
  disabled: true
}, {
  label: __('Zoom In'),
  value: 'zoomIn'
}, {
  label: __('Zoom In Down'),
  value: 'zoomInDown'
}, {
  label: __('Zoom In Left'),
  value: 'zoomInLeft'
}, {
  label: __('Zoom In Right'),
  value: 'zoomInRight'
}, {
  label: __('Zoom In Up'),
  value: 'zoomInUp'
}, {
  label: __('Zooming Exits'),
  value: null,
  disabled: true
}, {
  label: __('Zoom Out'),
  value: 'zoomOut'
}, {
  label: __('Zoom Out Down'),
  value: 'zoomOutDown'
}, {
  label: __('Zoom Out Left'),
  value: 'zoomOutLeft'
}, {
  label: __('Zoom Out Right'),
  value: 'zoomOutRight'
}, {
  label: __('Zoom Out Up'),
  value: 'zoomOutUp'
}, {
  label: __('Sliding Entrances'),
  value: null,
  disabled: true
}, {
  label: __('Slide In Down'),
  value: 'slideInDown'
}, {
  label: __('Slide In Left'),
  value: 'slideInLeft'
}, {
  label: __('Slide In Right'),
  value: 'slideInRight'
}, {
  label: __('Slide In Up'),
  value: 'slideInUp'
}, {
  label: __('Sliding Exits'),
  value: null,
  disabled: true
}, {
  label: __('Slide Out Down'),
  value: 'slideOutDown'
}, {
  label: __('Slide Out Left'),
  value: 'slideOutLeft'
}, {
  label: __('Slide Out Right'),
  value: 'slideOutRight'
}, {
  label: __('Slide Out Up'),
  value: 'slideOutUp'
}];
/**
 * Add mobile visibility controls on Advanced Block Panel.
 * @param {function} BlockEdit Block edit component.
 * @return {function} BlockEdit Modified block edit component.
 */

exports.styleControlOptions = styleControlOptions;
exports.delayControlOptions = delayControlOptions;
exports.speedControlOptions = speedControlOptions;
var withAdvancedControls = createHigherOrderComponent(function (BlockEdit) {
  return function (props) {
    var attributes = props.attributes,
        setAttributes = props.setAttributes,
        isSelected = props.isSelected;
    var speed = attributes.speed,
        delay = attributes.delay,
        style = attributes.style;
    return React.createElement(Fragment, null, React.createElement(BlockEdit, props), isSelected && React.createElement(InspectorControls, null, React.createElement(PanelBody, {
      title: __('Animations'),
      initialOpen: false
    }, React.createElement(SelectControl, {
      label: __('Style'),
      value: style,
      options: styleControlOptions,
      onChange: function onChange(selectedStyleOption) {
        setAttributes({
          style: selectedStyleOption
        });
      }
    }), React.createElement(SelectControl, {
      label: __('Speed'),
      value: speed,
      options: speedControlOptions,
      initialOpen: false,
      onChange: function onChange(selectedSpeedOption) {
        setAttributes({
          speed: selectedSpeedOption
        });
      }
    }), React.createElement(SelectControl, {
      label: __('Delay'),
      value: delay,
      options: delayControlOptions,
      initialOpen: false,
      onChange: function onChange(selectedDelayOption) {
        setAttributes({
          delay: selectedDelayOption
        });
      }
    }))));
  };
}, 'withAdvancedControls');
/**
 * Add custom element class in save element.
 * @param {Object} extraProps     Block element.
 * @param {Object} blockType      Blocks object.
 * @param {Object} attributes     Blocks attributes.
 * @return {Object} extraProps Modified block element.
 */

function applyExtraClass(extraProps, blockType, attributes) {
  var speed = attributes.speed,
      delay = attributes.delay,
      style = attributes.style;

  if (typeof style !== 'undefined' && '' !== style) {
    extraProps.className = (0, _classnames.default)(extraProps.className, 'animate__animated animate__' + style);
  }

  if (typeof speed !== 'undefined' && '' !== speed) {
    extraProps.className = (0, _classnames.default)(extraProps.className, 'animate__' + speed);
  }

  if (typeof delay !== 'undefined' && '' !== delay) {
    extraProps.className = (0, _classnames.default)(extraProps.className, 'animate__' + delay);
  }

  return extraProps;
}

addFilter('blocks.registerBlockType', 'editorskit/custom-attributes', _addAttributes.addAttributes);
addFilter('editor.BlockEdit', 'editorskit/custom-advanced-control', withAdvancedControls);
addFilter('blocks.getSaveContent.extraProps', 'editorskit/applyExtraClass', applyExtraClass);
},{"classnames":"../../node_modules/classnames/index.js","./addAttributes":"addAttributes.js"}]},{},["blocks-animate.js"], null)
//# sourceMappingURL=/blocks-animate.js.map